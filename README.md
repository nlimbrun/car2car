# Car2car Communication

This bachelor thesis deals with the development of a low-cost prototype of a Car2Car communication system.The goal is an early warning of the driver of potential dangers through data exchange with other vehicles.The prototype is based on the single-board computer Raspberry Pi which is connected to the vehicle with the help of an OBD cable. It also has a GPS receiver to determine the location and a display to warn of hazards. The main focus of this work is on programming in the Python programming language, proving its feasibility, but also showing the need for further research.


